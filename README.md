# Enhance Edge Super-Resolution - EESR

This repository is an official implementation of the paper [**Image Super-Resolution Improved by Edge Information**](https://ieeexplore.ieee.org/document/8914550 "Image Super-Resolution Improved by Edge Information") from IEEE International Conference on Systems, Man and Cybernetics (SMC), Bari, Italy, 2019 and the Master's thesis [**Image Super-Resolution Improved by Edge Information Using Residual Neural Networks**](http://repositorio.unicamp.br/jspui/handle/REPOSIP/335895 "Image Super-Resolution Improved by Edge Information Using Residual Neural Networks") from Institute of Computer (IC) of University of Campinas (UNICAMP).

* Note: This code is based on the [EDSR-PyTorch](https://github.com/thstkdgus35/EDSR-PyTorch "Enhanced Deep Residual Networks for Single Image Super-Resolution") version.

Abstract:
    As in other knowledge domains, deep learning techniques have revolutionized the development of approaches to image super-resolution. Recent algorithms for addressing this problem have employed convolutional neural networks in multi-layered residual architectures and general loss functions. These structures (architectures and loss functions) are generic and do not address the main features of an image for human visual perception (luminance, contrast and structure), resulting in better images, however, with noise mainly at its edges. In this work, we present and evaluate a method, called Edge Enhanced Super Resolution (EESR), using a new residual neural network focusing on the edges of the image and a combination of loss functions: Peak Signal-to-Noise Ratio (PSNR), L1, Multiple-Scale Structural Similarity (MS-SSIM) and a new function based on the Pencil Sketch technique. As main contribution of this work, the proposed model aims to leverage the limits of image super-resolution, presenting an improvement of the results in terms of the SSIM metric and achieving promising results for the PSNR metric. The obtained experimental results show that the developed model is competitive when compared to the state of the art for the four data sets (Set05, Set14, B100, Urban100) evaluated for image super-resolution.

### 2x upscale results comparison

![](/figs/set14_ppt3.jpg)

![](/figs/set14_comic.jpg)

## Architecture

![](/figs/architecture_EESR.jpg)

## Dependencies
| Lib          | Version    |
|--------------|------------|
|torch         | 1.0.1.post2|
|torchvision   | 0.2.2.post3|
|torchsummary  | 1.5.1      | 
|scikit-image  | 0.14.2     |
|matplotlib    | 3.1.1      |
|imageio       | 2.4.1      |
|numpy         | 1.14.1     |
|tqdm          | 4.28.1     |
|Pillow        | 5.0.0      |
|scipy         | 1.0.1      |

The command `pip3 install -r requeirements.txt` in the root directory, installs all necessary dependencies.

## Run code

### Directory tree:

- __benchmark__
  - __B100__
  - __Set5__
  - __Set14__
  - __Urban100__
- __DIV2K__
  - __DIV2k_train_HR__
  - __DIV2K_train_LR_bicubic__
- __figs__
- __src__
- README.md
- requirements.txt


The main parameters to run the code are:

* train_model - The name folder to save the results.
* loss - The loss functions used to train. You can use more one function and set the weights.
* list_unsharp - The layers to use de Residual Unsharp Block (RUB)
* data_test - The datasets used to test.
* epochs - The number of train epochs.
* dir_data - The dataset diretory.
* save_results - The diretory to create the results folder.
* scale - The super resoltion scale.
* n_resblocks - The total number of resudual blocks (with without RUB).

Example command:

`python3 main.py --train_model='500_all_loss_set_2X' --loss 1*L1+1*PencilSketch+1*PSNR+1*MS_SSIM --list_unsharp='1,2,3,4,5' --data_test Set5 --epochs 500 --ext --dir_data='../' --save_results --scale 2 --n_resblocks=32 --self_ensemble`

## Cite

Please to city our work in your research or publication use the bibtex: 

Paper: 
```
@INPROCEEDINGS{Galindo2019,
    author      = {E. {Galindo} and H. {Pedrini}},
    booktitle   = {2019 IEEE International Conference on Systems, Man and Cybernetics (SMC)},
    title       = {Image Super-Resolution Improved by Edge Information},
    year        = {2019},
    pages       = {3383-3389},
    doi         = {10.1109/SMC.2019.8914550},
    ISSN        = {1062-922X},
    month       = {Oct},
}
```

Master's Thesis:
```
@mastersthesis{Galindo2019,
  author       = {Eldrey S. Galindo}, 
  title        = {Image Super-Resolution Improved by Edge Information Using Residual Neural Networks},
  school       = {Universidade Estadual de Campinas, Instituto de Computação},
  year         = 2019,
  address      = {Campinas-SP, Brazil},
  month        = 12,
  page         = {64}
}
```
