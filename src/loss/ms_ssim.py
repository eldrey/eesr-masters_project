import utility

import torch
import torch.nn as nn
from torch.autograd import Variable
import pytorch_msssim


class MS_SSIM(nn.Module):
    def __init__(self):
        super(MS_SSIM, self).__init__()

    def forward(self, sr, hr):        
        
        loss = Variable(pytorch_msssim.msssim(sr, hr)+1, requires_grad = True)

        return 2-loss