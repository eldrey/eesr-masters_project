import utility

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import math


class PSNR(nn.Module):
    def __init__(self, rgb_range):
        super(PSNR, self).__init__()
        self.rgb_range = rgb_range


    def forward(self, sr, hr):

        def _forward(sr, hr, scale, rgb_range, orig=False):
            diff = (sr - hr).data.div(rgb_range)
            shave = scale
            if diff.size(1) > 1:
                convert = diff.new(1, 3, 1, 1)
                convert[0, 0, 0, 0] = 65.738
                convert[0, 1, 0, 0] = 129.057
                convert[0, 2, 0, 0] = 25.064
                diff.mul_(convert).div_(256)
                diff = diff.sum(dim=1, keepdim=True)

            valid = diff[:, :, shave:-shave, shave:-shave]
            mse = valid.pow(2).mean()


            return 100.0 - (-torch.mul(torch.log10(mse),10))
            

        
        loss = Variable(_forward(sr, hr, 2, self.rgb_range), requires_grad = True)

        return loss