
import utility

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torchvision

from math import exp


def yCbCr2rgb( yCbCr):
    #https://en.wikipedia.org/wiki/YCbCr
    rgb = torch.autograd.Variable(yCbCr.data.new(*yCbCr.size()))
    y  = yCbCr[:,0,:,:]
    Cb = yCbCr[:,1,:,:]
    Cr = yCbCr[:,2,:,:]
    aux1 = 255/219*(y - 16)
    aux2 = 255/224
    aux3 = 0.114/0.587
    aux4 = 0.299/0.587

    R = aux1 + aux2 * 1.402 * (Cr - 128)
    G = aux1 - aux2 * 1.772 * aux3 * (Cb - 128) - aux2 * 1.402 * aux4 * (Cr - 128)
    B = aux1 + aux2 * 1.772 * (Cb - 128)
    
    rgb[:,0,:,:] = R
    rgb[:,1,:,:] = G
    rgb[:,2,:,:] = B
    return rgb

def rgb2ycbcr(rgb):
    #https://en.wikipedia.org/wiki/YCbCr
    ycbcr = torch.autograd.Variable(rgb.data.new(*rgb.size()))

    R = rgb[:,0,:,:]
    G = rgb[:,1,:,:]
    B = rgb[:,2,:,:]

    Y   = 16 + (65.738 * R + 129.057 * G + 25.064 * B) / 256
    Cb  = 128 - (37.945 * R + 74.494 * G + 112.439 * B) / 256
    Cr  = 128 + (112.439 * R + 94.154 * G + 18.285 * B) / 256

    ycbcr[:,0,:,:] = Y
    ycbcr[:,0,:,:] = Cb
    ycbcr[:,0,:,:] = Cr

    return ycbcr


def save_image(img, obs_name, channel=3, orig_img=None):
    if channel == 1:
        img2save = orig_img.clone()
        img2save[:,0,:,:] = img
    else:
        img2save = img.clone()
    torchvision.utils.save_image(img2save, 'img_ps_'+obs_name+'.png', nrow=8, padding=2, normalize=False, range=None, scale_each=False, pad_value=0)

def gaussian(window_size, sigma):
    gauss = torch.Tensor([exp(-(x - window_size//2)**2/float(2*sigma**2)) for x in range(window_size)])
    return gauss/gauss.sum()


def create_window(window_size, channel=1):
    _1D_window = gaussian(window_size, 1.0).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = _2D_window.expand(channel, 1, window_size, window_size).contiguous()
    return window

#     sim, cs = ssim(img1, img2, window_size=11, size_average=True, full=True, val_range=None)
def gaussian_apply(img1, window_size=11, window=None, val_range=None):
    # print('gaussian_apply img1.size() = {}'.format(img1.size()))

    padd = int((window_size - 1) / 2)
    (_, channel, height, width) = img1.size()
    #([16, 3, 192, 192])
    if window is None:
        real_size = min(window_size, height, width)
        window = create_window(real_size, channel=channel).to(img1.device)

    mu1 = F.conv2d(img1, window, padding=padd, groups=channel)
    #save_image(mu1,'mu1_hr')

    # print('gaussian_apply mu1.size() = {}'.format(mu1.size()))
    mu1_sq = mu1.pow(2)
    # https://homepages.inf.ed.ac.uk/rbf/HIPR2/unsharp.htm
    sigma1_sq = F.conv2d(img1 * img1, window, padding=padd, groups=channel) - mu1_sq
    #save_image(sigma1_sq,'sigma1_sq_hr')

    return sigma1_sq

def pencil_sketch(tensor_img_orig, tensor_img_blur, rgb_range, channel_loss):
    img_blur = tensor_img_blur[:,channel_loss,:,:]
    #save_image(tensor_img_blur,'tensor_img_blur_hr')

    img_y = tensor_img_orig[:,channel_loss,:,:]
    ## save_image(img_y,'img_y_hr')

    img_blend = torch.div(img_blur, img_y)#cv2.divide(img_gray, img_blur, scale=256)
    #save_image(img_blend,'img_blend_hr', channel=1, orig_img=tensor_img_orig)

    #normalize
    img_blend_norm = torch.mul(img_blend, (255 / rgb_range))
    #save_image(img_blend_norm,'img_blend_norm_hr', channel=1, orig_img=tensor_img_orig)

    img_result = torch.mul(img_blend_norm, img_y)#cv2.multiply(img_yCrCb[:,:,0], img_blend, scale=1./256)
    #save_image(img_result,'img_result_hr', channel=1, orig_img=tensor_img_orig)

    #normalize
    img_result_norm = torch.mul(img_result, (255 / rgb_range))
    #save_image(img_result_norm,'img_result_norm_hr', channel=1, orig_img=tensor_img_orig)

    tensor_img_orig[:,channel_loss,:,:] = img_result_norm
    #save_image(tensor_img_orig,'tensor_img_orig_hr')
    return tensor_img_orig

def psnr(sr, hr, scale, rgb_range, orig=False):
        nan_sr = torch.sum(torch.isnan(sr))
        nan_hr = torch.sum(torch.isnan(hr))
        
        if nan_sr > 0:
            # print('Exist NAN in sr {}'.format(nan_sr))
            sr[sr != sr] = 0
        if nan_hr > 0:
            # print('Exist NAN in hr {}'.format(nan_hr))
            hr[hr != hr] = 0
        diff = (sr - hr).data.div(rgb_range)
        shave = scale
        if diff.size(1) > 1:
            convert = diff.new(1, 3, 1, 1)
            convert[0, 0, 0, 0] = 65.738
            convert[0, 1, 0, 0] = 129.057
            convert[0, 2, 0, 0] = 25.064
            diff.mul_(convert).div_(256)
            diff = diff.sum(dim=1, keepdim=True)

        valid = diff[:, :, shave:-shave, shave:-shave]
        # print('valid {}'.format(valid))
        mse = valid.pow(2)
        # print('valid.pow(2) {}'.format(mse))
        mse = mse.mean()
        # print("PSNR MSE {}".format(mse))


        return 100.0 - (-torch.mul(torch.log10(mse),10))

class Pencil_Sketch(nn.Module):
    def __init__(self, rgb_range, space_loss, channel_loss, func_ps_loss):
        super(Pencil_Sketch, self).__init__()
        self.loss_fn         = nn.MSELoss()
        self.rgb_range       = rgb_range
        self.space_loss      = space_loss        
        self.channel_loss    = channel_loss
        self.func_ps_loss    = func_ps_loss

    def _forward(self, sr, hr, channel_loss):

        ####### HR processing #######

        hr_blur = Variable(gaussian_apply(hr.clone()), requires_grad = True)
        hr_ps = pencil_sketch(hr.clone(), hr_blur, self.rgb_range, channel_loss)
        
        ####### SR processing #######
        sr_blur = Variable(gaussian_apply(sr.clone()), requires_grad = True)
        sr_ps = pencil_sketch(sr.clone(), sr_blur, self.rgb_range, channel_loss)

        loss = Variable(psnr(sr_ps, hr_ps, 2, self.rgb_range), requires_grad = True)
        return loss

    def forward(self, sr, hr):

        
        if self.space_loss == 'rgb':
            if self.channel_loss > 3:
                loss_R = self._forward(sr, hr, channel_loss=0)
                loss_G = self._forward(sr, hr, channel_loss=1)
                loss_B = self._forward(sr, hr, channel_loss=2)
                if self.func_ps_loss == 'max':
                    loss =  max(loss_R, loss_G, loss_B)
                elif self.func_ps_loss == 'sum':
                    loss = sum([loss_R, loss_G, loss_B])
            else:
                loss = self._forward(sr, hr, self.channel_loss)

        elif self.space_loss == 'ycbcr':
            sr_ycbcr = rgb2ycbcr(sr)
            hr_ycbcr = rgb2ycbcr(hr)
            loss = self._forward(sr_ycbcr, hr_ycbcr, self.channel_loss)

        
        return loss

