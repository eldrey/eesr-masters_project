import utility
from model import common
from loss import discriminator

import torch
import torch.nn as nn
import torch.nn.functional as F2
import torch.optim as optim
from torch.autograd import Variable

import torchvision

# import cv2

from math import exp
import numpy as np


def gaussian(window_size, sigma):
    # Create gaussian kernels
    # kernel = Variable(torch.FloatTensor([[0.006, 0.061, 0.242, 0.383, 0.242, 0.061, 0.006]]))
    
    
    # # Apply smoothing
    # return F.conv1d(img, kernel)


    # # Create gaussian kernels
    # kernel = Variable(torch.FloatTensor(
    #     [
    #         [1, 4,  7,  4,  1],
    #         [4, 16, 26, 16, 4],
    #         [7, 26, 41, 26, 7],
    #         [4, 16, 26, 16, 4],
    #         [1, 4,  7,  4,  1]
    #     ]))#, requires_grad = False).cuda()


    # # Apply smoothing
    # return F.conv1d(img, kernel)

    gauss = torch.Tensor([exp(-(x - window_size//2)**2/float(2*sigma**2)) for x in range(window_size)])
    return gauss/gauss.sum()

def create_window(window_size, channel=1):
    _1D_window = gaussian(window_size, sigma=1.5).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = _2D_window.expand(channel, 1, window_size, window_size).contiguous()
    return window

def _pencil_sketch(img):
    window_size = 5

    if torch.max(img) > 128:
        max_val = 255
    else:
        max_val = 1

    if torch.min(img) < -0.5:
        min_val = -1
    else:
        min_val = 0
    L = max_val - min_val

    (_, channel, height, width) = img.size()
    real_size = min(window_size, height, width)
    window = create_window(real_size, channel=channel).to(img.device).type(img.dtype)

    img_blur = F2.conv2d(img, window, padding=0, groups=1)
    img_blur = img_blur[:,:,0]

    img_y = img[:,:,0]


    # img_blur = gaussianBlur(img_y)#, (5, 5), 0, 0)#(21, 21), 0, 0)

    img_blend = torch.div(img_blur, img_y)#cv2.divide(img_gray, img_blur, scale=256)
    #normalize
    img_blend_norm = torch.mul(img_blend, (255 / self.rgb_range))

    img_result = torch.mul(img_blend_norm, img_y)#cv2.multiply(img_yCrCb[:,:,0], img_blend, scale=1./256)
    #normalize
    img_result_norm = torch.mul(img_result, (255 / self.rgb_range))
    
    return img_result_norm

class Pencil_Sketch(nn.Module):
    def __init__(self, rgb_range, window_size=11, size_average=True, val_range=None):
        super(Pencil_Sketch, self).__init__()
        self.rgb_range = rgb_range
        self.window_size = window_size
        self.size_average = size_average
        self.val_range = val_range

        # Assume 1 channel for SSIM
        self.channel = 1
        self.window = create_window(window_size)



    def forward(self, sr, hr):

        (_, channel, _, _) = sr.size()

        if channel == self.channel and self.window.dtype == sr.dtype:
            window = self.window
        else:
            window = create_window(self.window_size, channel).to(sr.device).type(sr.dtype)
            self.window = window
            self.channel = channel



        # to_pil = torchvision.transforms.ToPILImage()
        print("SR")
        sr_ps = _pencil_sketch(sr)
        print("HR")
        hr_ps = _pencil_sketch(hr)

        diff = Variable(hr_ps - sr_ps, requires_grad = True)
        diff = torch.mul(diff, (255 / self.rgb_range))
        print('loss Pencil_Sketch {}'.format(diff))
        diff = diff.mean()
        print('loss Pencil_Sketch {}'.format(type(diff)))
        loss =diff#Variable(hr_ps - sr_ps, requires_grad = True)

        return loss