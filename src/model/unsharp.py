import math

import torch
import torch.nn as nn
import torch.nn.functional as F

from math import exp
from torch.autograd import Variable

class Gaussian(nn.Module):
    def __init__(
        self, in_channels, out_channels, group=1, kernel_size=5, bias=True, sigma=1.0):
        m = []
        super(Gaussian, self).__init__(*m)
        
        gaussian_kernel = torch.Tensor([exp(-(x - kernel_size//2)**2/float(2*sigma**2)) for x in range(kernel_size)])
        gaussian_kernel = gaussian_kernel / gaussian_kernel.sum()
        gaussian_kernel = gaussian_kernel.repeat(kernel_size).view(kernel_size, kernel_size)
        gaussian_kernel_f = gaussian_kernel * gaussian_kernel.t()
 
        gaussian_kernel = gaussian_kernel.view(group, 1, kernel_size, kernel_size)

        gaussian_filter = nn.Conv2d(in_channels=in_channels, out_channels=out_channels,
                    kernel_size=kernel_size, groups=group, bias=False, padding=int((kernel_size - 1) / 2))
        gaussian_filter.weight.data = gaussian_kernel
        self.alpha = 1.5
        self.beta = -0.5
        self.sigma = 1.0
        self.gamma = 0
        self.body = nn.Sequential(*m)


    def forward(self, x):
        res = torch.cuda.FloatTensor(x.shape)
        for i in range(len(x)):
            img2 = x[i].unsqueeze(0)[0,0]

            h, w = img2.shape
            img3 = img2.view(1,1,h,w)

            x_gaussian = self.body(img3)
            res[i] = img3*self.alpha + x_gaussian*self.beta + self.gamma
        res += x
        return res
