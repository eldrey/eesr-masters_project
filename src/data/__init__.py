from importlib import import_module

from dataloader import MSDataLoader
from torch.utils.data.dataloader import default_collate

class Data:
    def __init__(self, args):
        self.loader_train = None
        self.loader_test = []
        testset = []
        if not args.test_only:
            module_train = import_module('data.' + args.data_train.lower())
            trainset = getattr(module_train, args.data_train)(args)
            self.loader_train = MSDataLoader(
                args,
                trainset,
                batch_size=args.batch_size,
                shuffle=True,
                pin_memory=not args.cpu
            )

        for data_test in args.data_test:
            if data_test in ['Set5', 'Set14', 'B100', 'Urban100', 'Div2K_test']:
                module_test = import_module('data.benchmark')
                testset.append(getattr(module_test, 'Benchmark')(args, data_test=data_test, train=False))
            else:
                module_test = import_module('data.' +  data_test.lower())
                testset.append(getattr(module_test, data_test)(args, train=False))

        for test_set in testset:
            self.loader_test.append(MSDataLoader(
                args,
                test_set,
                batch_size=1,
                shuffle=False,
                pin_memory=not args.cpu
            ))

