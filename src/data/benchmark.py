import os

from data import common
from data import srdata

import numpy as np

import torch
import torch.utils.data as data

class Benchmark(srdata.SRData):
    def __init__(self, args, data_test, name='', train=True, benchmark=True):
        super(Benchmark, self).__init__(
            args=args, data_test=data_test, name=name, train=train, benchmark=True
        )

    def _set_filesystem(self, dir_data, data_test):
        self.apath = os.path.join(dir_data, 'benchmark', self.name)

        self.dir_hr = os.path.join(self.apath+data_test, 'HR/')
        self.dir_lr = os.path.join(self.apath+data_test, 'LR_bicubic/')
        self.ext = ('', '.png')

