import random

import numpy as np
import skimage.color as sc

import torch
from torchvision import transforms

def get_patch(*args, patch_size=96, scale=1, multi_scale=False):
    ih, iw = args[0].shape[:2]

    p = scale if multi_scale else 1
    tp = p * patch_size
    ip = tp // scale

    ix = random.randrange(0, iw - ip + 1)
    iy = random.randrange(0, ih - ip + 1)
    tx, ty = scale * ix, scale * iy

    ret = [
        args[0][iy:iy + ip, ix:ix + ip, :],
        *[a[ty:ty + tp, tx:tx + tp, :] for a in args[1:]]
    ]

    return ret

def set_channel(*args, color_space, n_channels=3):
    def _set_channel(img):
        if img.ndim == 2:
            img = np.expand_dims(img, axis=2)

        c = img.shape[2]
        if n_channels == 1 and c == 3:
            img = np.expand_dims(sc.rgb2ycbcr(img)[:, :, 0], 2)
        elif n_channels == 3 and c == 1:
            img = np.concatenate([img] * n_channels, 2)
        elif color_space == 'ycbcr':
            img = sc.rgb2ycbcr(img)
            if c == 1:
                img = np.concatenate([img] * n_channels, 2)

        return img

    return [_set_channel(a) for a in args]

def np2Tensor(*args, rgb_range=255):
    def _np2Tensor(img):
        np_transpose = np.ascontiguousarray(img.transpose((2, 0, 1)))
        tensor = torch.from_numpy(np_transpose).float()
        tensor.mul_(rgb_range / 255)

        return tensor

    return [_np2Tensor(a) for a in args]

# import cv2

def augment(*args, hflip=True, rot=True):
    import skimage
    from skimage.filters import roberts, sobel, scharr, prewitt
    hflip = hflip and random.random() < 0.5
    vflip = rot and random.random() < 0.5
    rot90 = rot and random.random() < 0.5
    # add_ch = random.random() < 0.5
    # sob = random.random() < 0.5
    

    def _augment(img):
        if hflip: img = img[:, ::-1, :]
        if vflip: img = img[::-1, :, :]
        if rot90: img = img.transpose(1, 0, 2)
        # if add_ch: img = skimage.exposure.equalize_hist(img)
        # if sob: img = sobel(img[:,:,0])
        
        
        return img
    list_i = [_augment(a) for a in args]
    # print('list_i {}'.format(len(list_i)))
    # for img in list_i:
    #     cv2.imshow('image',img)
    #     cv2.waitKey(0)
    # img1, img2 = args
    # if hflip: img1 = img1[:, ::-1, :]
    # if vflip: img1 = img1[::-1, :, :]
    # if rot90: img1 = img1.transpose(1, 0, 2)

    # if hflip: img2 = img2[:, ::-1, :]
    # if vflip: img2 = img2[::-1, :, :]
    # if rot90: img2 = img2.transpose(1, 0, 2)

    # if add_ch: img1 = skimage.exposure.equalize_hist(img1)
    # if sob: img1 = sobel(img1[:,:,0])
    # list_i = [img1, img2]

    return list_i